package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/streadway/amqp"
	dsn "gitlab.com/sergio.segala/go-dsn-parser"
)

var dsnString = "amqp://guest:guest@localhost:5672/"
var quietMode = false
var initialDelay = 0
var finalDelay = 0
var maxRetry = -1
var retryDelay = 10

func log(format string, params ...interface{}) {
	if !quietMode {
		fmt.Printf(format+"\n", params...)
	}
}

func fatalError(format string, params ...interface{}) {
	if params == nil {
		fmt.Printf("%s\n", format)
	} else {
		fmt.Printf(format+"\n", params)
	}
	os.Exit(1)
}

func failOnError(msg string, err error) {
	if err != nil {
		fatalError(msg, err)
	}
}

func checkRetry(totRetries int) {
	if maxRetry == 0 {
		os.Exit(1)
	}
	if maxRetry >= 0 && totRetries > maxRetry {
		fatalError("Too many retry. Exit")
	}
}

func parseCommandLine() {

	flag.StringVar(&dsnString, "dsn", dsnString, "RabbitMQ DSN")
	flag.IntVar(&maxRetry, "retry", maxRetry, "Max retry number. (-1 infinite, 0 no retry)")
	flag.IntVar(&retryDelay, "retry-delay", retryDelay, "Seconds between retry")
	flag.IntVar(&initialDelay, "initial-delay", initialDelay, "Initial delay in seconds (before first connection)")
	flag.IntVar(&finalDelay, "final-delay", finalDelay, "Final delay in seconds (after connection established)")
	flag.BoolVar(&quietMode, "q", false, "Quiet mode")

	flag.Parse()
}

func main() {
	parseCommandLine()

	dsn, err := dsn.Parse(dsnString)
	failOnError("Error parsing dsn: %v", err)
	if dsn.Scheme != "amqp" {
		fatalError("Invalid dsn scheme: %s", dsn.Scheme)
	}

	if initialDelay > 0 {
		log("Waiting for RabbitMQ (%dsec)", initialDelay)
		time.Sleep(time.Duration(initialDelay) * time.Second)
	}

	totRetries := 0
	for {
		if dsn.Database == "" {
			log("Connecting to %s:%d as user %s", dsn.Host, dsn.Port, dsn.Username)
		} else {
			log("Connecting to %s:%d on vhost %s as user %s", dsn.Host, dsn.Port, dsn.Database, dsn.Username)
		}

		conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%d/%s", dsn.Username, dsn.Password, dsn.Host, dsn.Port, dsn.Database))
		if err != nil {
			log("Connecting error: %s", err)
			totRetries++
			checkRetry(totRetries)
			if maxRetry != 0 {
				log("Reconnect in %dsec", retryDelay)
				time.Sleep(time.Duration(retryDelay) * time.Second)
			}
			continue
		}
		defer conn.Close()
		log("Connected!")
		break
	}

	if finalDelay > 0 {
		log("Waiting %dsec", finalDelay)
		time.Sleep(time.Duration(finalDelay) * time.Second)
	}
}
