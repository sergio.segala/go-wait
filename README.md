# go-wait

## go-wait-rabbitmq

Wait until rabbit is up

```
Usage:
    go-wait-rabbitmq [dsn string]  [-retry-delay n] [-retry n] [-initial-delay n] [-final-delay n] [-q]
    -dsn string             RabbitMQ DSN (default "amqp://guest:guest@localhost:5672/")
    -retry int              Max retry number. (-1 infinite, 0 no retry) (default -1)
    -retry-delay int        Seconds between retry (default 10)
    -initial-delay int      Initial delay in seconds (before first connection)
    -final-delay int        Final delay in seconds (after connection established)
    -q                      Quiet mode
```
