FROM golang:1.13-alpine3.11 as builder

WORKDIR /app

RUN apk update && apk upgrade && \
    apk add --no-cache git openssh-client ca-certificates

COPY . .

RUN go mod download
RUN CGO_ENABLED=0 go build -mod=readonly -v -ldflags '-extldflags "-static"' -o go-wait-rabbitmq cmd/go-wait-rabbitmq.go

# Final container
FROM scratch
COPY --from=builder /app/go-wait-rabbitmq /bin/go-wait-rabbitmq
ENTRYPOINT ["/bin/go-wait-rabbitmq"]
