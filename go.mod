module gitlab.com/sergio.segala/go-wait

go 1.13

require (
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71 // indirect
	gitlab.com/sergio.segala/go-dsn-parser v0.0.0 // indirect
)
